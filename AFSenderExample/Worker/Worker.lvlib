﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Abstract Messages for Caller" Type="Folder"/>
	<Item Name="Messages for this Actor" Type="Folder">
		<Item Name="CheckAllComplete Msg.lvclass" Type="LVClass" URL="../Worker Messages/CheckAllComplete Msg/CheckAllComplete Msg.lvclass"/>
		<Item Name="LongTimeProcess Msg.lvclass" Type="LVClass" URL="../Worker Messages/LongTimeProcess Msg/LongTimeProcess Msg.lvclass"/>
		<Item Name="LoopWork Msg.lvclass" Type="LVClass" URL="../Worker Messages/LoopWork Msg/LoopWork Msg.lvclass"/>
		<Item Name="MethodA Msg.lvclass" Type="LVClass" URL="../Worker Messages/MethodA Msg/MethodA Msg.lvclass"/>
		<Item Name="MethodB Msg.lvclass" Type="LVClass" URL="../Worker Messages/MethodB Msg/MethodB Msg.lvclass"/>
		<Item Name="MethodC Msg.lvclass" Type="LVClass" URL="../Worker Messages/MethodC Msg/MethodC Msg.lvclass"/>
		<Item Name="MethodD Msg.lvclass" Type="LVClass" URL="../Worker Messages/MethodD Msg/MethodD Msg.lvclass"/>
		<Item Name="QueryWorkTimeMinMax Msg.lvclass" Type="LVClass" URL="../Worker Messages/QueryWorkTimeMinMax Msg/QueryWorkTimeMinMax Msg.lvclass"/>
		<Item Name="Read Voltage Msg.lvclass" Type="LVClass" URL="../Worker Messages/Read Voltage Msg/Read Voltage Msg.lvclass"/>
		<Item Name="Read WorkTimeResult Msg.lvclass" Type="LVClass" URL="../Worker Messages/Read WorkTimeResult Msg/Read WorkTimeResult Msg.lvclass"/>
		<Item Name="RegisterSubscriptor Msg.lvclass" Type="LVClass" URL="../Worker Messages/RegisterSubscriptor Msg/RegisterSubscriptor Msg.lvclass"/>
		<Item Name="UnregisterSubscriptor Msg.lvclass" Type="LVClass" URL="../Worker Messages/UnregisterSubscriptor Msg/UnregisterSubscriptor Msg.lvclass"/>
		<Item Name="VoltageMeasurement Msg.lvclass" Type="LVClass" URL="../Worker Messages/VoltageMeasurement Msg/VoltageMeasurement Msg.lvclass"/>
	</Item>
	<Item Name="Worker.lvclass" Type="LVClass" URL="../Worker/Worker.lvclass"/>
</Library>
